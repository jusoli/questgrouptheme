var i = 1;
var prog = 0;
$( document ).ready(function() {
  $("#proximo").click(function(){
    if ($.trim($('.contato .entrada').eq(i-1).val()) !== '') {
      prog = ( $(".contato").width() / 3 ) * i;
      $(".progresso").animate({
        width: prog
      });
      $(".contato label").eq(i).show(400);
      $(".contato .entrada").eq(i).show();
      $(".contato label").eq(i-1).hide(400);
      $(".contato .entrada").eq(i-1).hide();
      i++;
      if (i > 3) {
        $(".contato input[type='button']").hide();
        $(".mensagem").fadeIn(600);
        $(".barra").fadeOut(400);
        $(".pos_numero").fadeOut(400);
        $("#alerta").fadeOut(400);
      }
      if (i < 4) {
          $("#numero").html(i);
      }
    }else{
      $("#alerta").html("Please, verify your information.")
    }
  });
});
